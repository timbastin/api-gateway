import Axios, {AxiosResponse} from "axios";
import {StatusProtocol} from "../interfaces";

export class Service implements StatusProtocol {
    status: number = null;
    cpuLoad: string = null;
    freeMem: number = null;
    totalMem: number = null;
    statusCounter = 0;

    constructor(public type: string, public ip: string, public port: number) {}

    public async getValues(): Promise<void> {
        const res: AxiosResponse<StatusProtocol> = await Axios.get(this.ip + ":" + this.port + "/monitoring");
        console.log(this.type + " has HTTP status: " + res.status);
        console.log(this.type + " has a current CPU Usage of : " + res.data.cpuLoad + "%");
        console.log(this.type + " has a total memory of:" + this.totalMem + "MB");
        console.log(this.type + " has an available memory of:" + this.freeMem + "MB");
        this.status = res.status;
        this.cpuLoad = res.data.cpuLoad;
        this.totalMem = res.data.totalMem;
        this.freeMem = res.data.freeMem;
        this.port = res.data.port;
    }
}
