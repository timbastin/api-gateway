
import App from "./App";
import AuthController from "./controllers/AuthController";
import dotenv from "dotenv";
import {RegisterController} from "./controllers/RegisterController";
import GateController from "./controllers/GateController";

/**
 * Start Express server.
 */
dotenv.config();
const app = new App(
    [
        new AuthController(),
        new RegisterController(),
        new GateController(),
    ],
    +process.env.PORT || 3000,
);

app.listen();
