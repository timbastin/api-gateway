import * as express from "express";

export interface Response<T> extends express.Response {
    send: (body: T) => express.Response;
}
