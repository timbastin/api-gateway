import ServiceContainer from "../ServiceContainer";

export default class StatusManager {
    static init() {
        setInterval(() => {
            console.log(ServiceContainer.services);
            Object.keys(ServiceContainer.services).forEach(async key => {
                // iterate over each single service of the given type.
                await Promise.all([ServiceContainer.services[key].map(service => {
                    try {
                        service.getValues();
                        console.log("Service state requested. Type:"+ service.type+" IP: "+service.ip );
                        service.statusCounter = 0;
                    } catch (err) {
                        console.log("No response. Error :" + err);
                        service.statusCounter = service.statusCounter + 1;
                        if (service.statusCounter === 3) {
                            ServiceContainer.removeService(service);
                        }
                    }
                })]);
            });
        }, 2000);
    }
}
