import * as fs from "fs";
import jwt from "jsonwebtoken";
import path from "path";
import {Token} from "../interfaces";

export default class AuthService {
    static privateKey: Buffer = fs.readFileSync(path.join(__dirname, "..", "..", "keys", "key.pem"));
    static publicKey: Buffer = fs.readFileSync(path.join(__dirname, "..", "..", "keys", "public.pem"));

    /**
     * Throws error when token is not valid.
     * @throws
     * @param token
     */
    public static verifyToken(token: string): object | string {
        return jwt.verify(token, AuthService.publicKey);
    }

    public static issueToken(claims: Record<string, string | number>): Token {
        // generate token using private key.
        return {
            // eslint-disable-next-line @typescript-eslint/camelcase
            access_token: jwt.sign(claims, AuthService.privateKey, { algorithm: "RS256", expiresIn: 60 * 60 * 24}),
            // eslint-disable-next-line @typescript-eslint/camelcase
            token_type: "Bearer",
            // eslint-disable-next-line @typescript-eslint/camelcase
            expires_in: 60 * 60 * 24
        };
    }
}
