import * as express from "express";
export default abstract class Controller {
    public abstract path: string;
    public abstract router: express.Router;

    public abstract initializeRoutes(): void;
}
