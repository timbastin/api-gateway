import Controller from "./Controller";
import * as express from "express";
import Axios, {AxiosError} from "axios";
import {Request} from "../requests";
import {Response} from "../responses";
import ServiceContainer from "../ServiceContainer";

interface BridgeProtocol {
    data: any;
}

export default class GateController extends Controller {
    path = "/protected/bridge";
    router =  express.Router();

    constructor() {
        super();
        this.initializeRoutes();
    }
    initializeRoutes(): void {
        this.router.get(this.path, this.route);
        this.router.patch(this.path, this.route);
        this.router.put(this.path, this.route);
        this.router.post(this.path, this.route);
        this.router.options(this.path, this.route);
    }

    private async route(request: Request<BridgeProtocol>, response: Response<any>) {

        if (!request.query["type"] || !request.query["path"]) {
            return response.status(400).json({msg: "Query Parameters 'type' and 'path' have to be defined."});
        }

        const {type, path} = request.query;
        const service = ServiceContainer.getService(type);

        if (!service) {
            return response.status(400).json({msg: "Unknown Service Type: " + type  });
        }

        try {
            let res;
            const url = "http://" + service.ip + ":" + service.port + "/" + path;
            switch (request.method.toLowerCase()) {
                case "patch":
                case "put":
                case "post":
                    res = await (Axios as any)[request.method.toLowerCase()](url, request.body.data, {
                        headers: {
                            "Content-Type": "application/json",
                            accept: "application/json",
                        }
                    });
                    break;
                case "get":
                    res = await Axios.get(url, {
                        headers: {
                            "Content-Type": "application/json",
                            accept: "application/json",
                        }
                    });
                    break;
            }
            response.status(res.status).send(res.data);
        } catch (e) {
            console.log(e);
            if ("code" in e) {
                // should be in axios error.
                response.status(+(e as AxiosError).code).send((e as AxiosError).message);
            } else {
                response.status(500).json({msg: "Yep, we fucked up"});
            }
        }
    }
}
