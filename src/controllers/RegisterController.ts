import Controller from "./Controller";
import express from "express";
import {Request} from "../requests";
import {RegisterProtocol} from "../interfaces";
import {Service} from "../classes/Service";
import ServiceContainer from "../ServiceContainer";
import {Response} from "../responses";

export class RegisterController extends Controller{
    public path = "/protected/register";
    public router: express.Router = express.Router();

    constructor() {
        super();
        this.initializeRoutes();
    }

    public initializeRoutes(): void {
        this.router.post(this.path, this.register);
    }

    private register(req: Request<RegisterProtocol>, response: Response<any>) {
        const {type, ip, port} = req.body;
        const service = new Service(type, ip, port);
        ServiceContainer.addService(service);
        response.sendStatus(201);
    }
}
