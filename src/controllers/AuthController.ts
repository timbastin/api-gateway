import Controller from "./Controller";
import * as express from "express";
import {Request} from "../requests";
import {Response} from "../responses";
import AuthService from "../services/AuthService";
import {IssueTokenProtocol, Token} from "../interfaces";

export default class AuthController extends Controller {
    public path = "/auth/token";
    public router = express.Router();

    public constructor() {
        super();
        this.initializeRoutes();
    }

    /**
     * Gets called when constructing the controller
     */
    public initializeRoutes(): void {
        this.router.post(this.path, this.issueToken);
    }

    public issueToken(request: Request<IssueTokenProtocol>, response: Response<Token>) {
        if (AuthController.verifyClientSecret(request.body)) {
            response.send(AuthService.issueToken({}));
        } else {
            // if the client secret is wrong we return a 403 error.
            response.sendStatus(403);
        }
    }

    private static verifyClientSecret(issueToken: IssueTokenProtocol): boolean {
        return process.env[issueToken.client_id] === issueToken.client_secret;
    }
}
