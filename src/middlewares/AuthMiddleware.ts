import {Middleware, Request} from "express-validator/src/base";
import AuthService from "../services/AuthService";
import {Response} from "express";

export const AuthMiddleware: Middleware = (req: Request, res: Response, next: (err?: any) => void) => {
    // check token.
    const authorizationHeader: string = req.headers["Authorization"] ? req.headers["Authorization"] : req.headers["authorization"];
    try {
        if (!authorizationHeader || !authorizationHeader.startsWith("Bearer ")) {
            throw new Error("Token does not start with Bearer prefix");
        }
        // currently the auth client is not used.
        // we could get that information right here.
        AuthService.verifyToken(authorizationHeader.substring(7));
        next();
    } catch (e) {
        res.status(401).send({msg: "Unauthorized. Double check your auth token."});
    }
};
