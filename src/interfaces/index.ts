export interface RegisterProtocol {
    type: string;
    ip: string;
    port: number;
}

export interface StatusProtocol {
    status: number;
    cpuLoad: string;
    freeMem: number;
    totalMem: number;
    port: number;
}

export interface IssueTokenProtocol {
    client_id: number;
    client_secret: string;
}

export interface Token {
    access_token: string;
    token_type: "Bearer";
    expires_in: number;
}
