import {Service} from "./classes/Service";

interface ServiceRegistry {
    [key: string]: Service[];
}

export default class ServiceContainer {
    public static services: ServiceRegistry = {};
    public static addService(service: Service): void {
        if (ServiceContainer.services[service.type]) {
            if (!ServiceContainer.services[service.type].some(s => s.ip !== service.ip && s.port !== service.port)) {
                ServiceContainer.services[service.type].push(service);
            }
            // service already exists.
        } else {
            ServiceContainer.services[service.type] = [service];
        }
    }

    /**
     * Removes a whole service type by the given type name.
     * @param {string} type Name of the given service type.
     */
    public static removeServiceType(type: string): void {
        ServiceContainer.services[type].forEach(Service => {
            ServiceContainer.services[type].splice(ServiceContainer.services[type].indexOf(Service));
        });
    }

    public static removeService(service: Service): void {  //löscht eine bestimmte Instanz
        ServiceContainer.services[service.type].splice(ServiceContainer.services[service.type].indexOf(service));
    }

    public static getService(type: string): Service | null {
        const services: Service[] = ServiceContainer.services[type];
        if (services && services.length > 0) {
            let minCpu: Service = services[0];
            services.forEach(service => {
                if (+minCpu.cpuLoad > +service.cpuLoad) {
                    minCpu = service;
                }
            });
            return minCpu;
        }
    }
}
