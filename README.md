# API Gateway H-BRS
# Table of contents:

- [Pre-reqs](#pre-reqs)
- [Getting started](#getting-started)
# Protocols
1. Obtaining a JSON Web Token for authorization and authentication:
    To receive a new token a `POST` request has to be made to the path: `<IP OF GATEWAY>:5000/auth/token`. The provided Data has to match the `IssueTokenProtocol`
   ```json
    {
        "client_id": "number",
        "client_secret": "string"
    }
   ```
   The `client_secret` and the `client_id` will be provided beforehand.
   The response will be of the format:
   ```json
    {
        "access_token": "string",
        "token_type": "Bearer",
        "expires_in": "number"
    }
   ```
   **In every further request the JSON Web Token has to be provided inside the `Authorization` Header.
   Example: Authorization: <token_type> <access_token>
2. Registering a new Service
   To register a new service a `POST` request has to be made to the endpoint: 
   `protected/register`. The Data has to be in the format:
   ```json
   {
    "type": "string",
    "ip": "string",
    "port": "number"
   }
   ```
   If a positive status is returned, the service registration was successful.
   
3. Forwarding Requests
   All requests to the endpoint `protected/bridge` will be forwarded. The query parameter `type` and `path` has to be defined. Otherwise a `400` status will get returned. Every forwarded data has to match the following format:
   ```json
    {
     "data": "any"
    }
   ```
4. Status Request/Response
   Periodically a status request will be send to the services endpoint `IP + : + Port + /monitoring `.
   The response has to inlcude `status`, `cpuLoad`,`freeMem`,`totalMem` and `port` in the following format.
   
   ```json
   {
       "status" : "number",
       "cpuLoad" : "string",     // without % !
       "freeMem" : "number",   // in MB
       "totalMem" : "number",
       "port": "number"
   }
   ```
# Contribute to the project
## Pre-reqs
To build and run this app locally you will need a few things:
- Install [Node.js](https://nodejs.org/en/)
- Install [git](https://git-scm.com/book/de/v1/Los-geht%E2%80%99s-Git-installieren)

## Getting started
- Clone the repository
    ```
    git clone https://gitlab.com/bastin.tim/api-gateway.git
    ```
- Install dependencies
    ```
    cd api-gateway
    npm install
    ```
- Run the project
    ```
    npm start
    ```
